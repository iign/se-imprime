# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Germán Dotta -- Designer -- @grmn
    Ignacio Toledo -- Developer -- @iign


# THANKS

    

# TECHNOLOGY COLOPHON

    HTML5, CSS3
    jQuery, Modernizr
    Python/Django, Postgres