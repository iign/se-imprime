<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Shop;
use App\Service;
use Image;
use Illuminate\Support\Str;

use DB;

use Validator, Input, Redirect, Session;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $q = Input::get('q');

        $shops = DB::table('shops')->where('name', 'LIKE', "%$q%")
                ->orderBy('name', 'asc')
                ->paginate(30);

        return view('admin.shops', ['shops' => $shops, 'query' => $q]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $shopServices = array();
        $rubros = Service::where('category_id', 1)->orderby('name', 'ASC')->get();
        $tareas = Service::where('category_id', 2)->orderby('name', 'ASC')->get();
        $piezas = Service::where('category_id', 3)->orderby('name', 'ASC')->get();
        $terminaciones = Service::where('category_id', 4)->orderby('name', 'ASC')->get();
        $materiales = Service::where('category_id', 5)->orderby('name', 'ASC')->get();

        $services = array(
            'Rubros' => $rubros,
            'Tareas' => $tareas,
            'Piezas' => $piezas,
            'Terminaciones' => $terminaciones,
            'Materiales' => $materiales
        );
        return view('admin.shop_create', array('services' => $services, 'shopServices' => $shopServices));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        // Validate
        $rules = array(
            'shop-name'      => 'required',
            'shop-address'   => 'required',
            'shop-phone'     => 'required',
            'shop-email'     => 'email'
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/shop/create')
                ->withErrors($validator)
                ->withInput();
        } else {

            // store
            $shop = new Shop();
            $shop->name = Input::get('shop-name');
            $shop->address = Input::get('shop-address');
            $shop->opening_hours = Input::get('shop-opening_hours');
            $shop->phone = Input::get('shop-phone');
            $shop->email = Input::get('shop-email');
            $shop->website = Input::get('shop-website');
            $shop->featured = Input::get('shop-featured') || false;

            $shop->lat = Input::get('shop-lat');
            $shop->lng = Input::get('shop-lng');

            $shop->slug = Str::slug(Input::get('shop-name'));

            // Shop logo
            if ($request->hasFile('shop-logo')) {
                $file = $request->file('shop-logo');
                $fileName = str_slug($shop->name);

                $extension = $file->getClientOriginalExtension();
                $fileNameResized = $fileName . '-400x230.' . $extension;
                $fileName = $fileName . '.' . $extension;
                $destinationPath = 'uploads/' . $fileNameResized;

                Image::make($file->getRealPath())->fit(400, 230)->save($destinationPath);
                $shop->logo = $fileNameResized;
            }

            $shop->save();

            $services = (Input::get('shopServices') ?: []);
            $shop->services()->sync($services);

            // redirect
            Session::flash('message', 'Empresa creada con éxito');
            return Redirect::to('admin/shop');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, $slug)
    {
        $shop = Shop::findOrFail($id);
        $rubros = $shop->services->where('category_id', 1);

        $meta_description = '';
        foreach ($shop->services as $key => $s) {
            $meta_description .= $s->name . ', ';
            if ($key > 10) {
                break;
            }
        }
        $shop->meta_description = rtrim($meta_description, ', ');

        return view('shop_view', array('shop' => $shop, 'rubros' => $rubros));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $shop = Shop::with('services')->findOrFail($id);

        $rubros = Service::where('category_id', 1)->orderby('name', 'ASC')->get();
        $tareas = Service::where('category_id', 2)->orderby('name', 'ASC')->get();
        $piezas = Service::where('category_id', 3)->orderby('name', 'ASC')->get();
        $terminaciones = Service::where('category_id', 4)->orderby('name', 'ASC')->get();
        $materiales = Service::where('category_id', 5)->orderby('name', 'ASC')->get();

        $services = array(
            'Rubros' => $rubros,
            'Tareas' => $tareas,
            'Piezas' => $piezas,
            'Terminaciones' => $terminaciones,
            'Materiales' => $materiales
        );

        $shopServices = array();
        foreach ($shop->services as $s) {
            $shopServices[] = $s->id;
        }

            return view('admin.shop_edit', array(
                'shop' => $shop,
                'services' => $services,
                'shopServices' => $shopServices
            ));
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // store
        $shop = Shop::findOrFail($id);
        $shop->name = Input::get('shop-name');
        $shop->slug = Str::slug(Input::get('shop-name'));
        $shop->address = Input::get('shop-address');
        $shop->opening_hours = Input::get('shop-opening_hours');
        $shop->phone = Input::get('shop-phone');
        $shop->email = Input::get('shop-email');
        $shop->website = Input::get('shop-website');
        $shop->featured = Input::get('shop-featured') || false;

        $shop->lat = Input::get('shop-lat');
        $shop->lng = Input::get('shop-lng');

        var_dump(Input::get('shopServices'));
        $services = (Input::get('shopServices') ?: []);
        $shop->services()->sync($services);

        // Shop logo
        if ($request->hasFile('shop-logo')) {
            $file = $request->file('shop-logo');
            $fileName = str_slug($shop->name);

            $extension = $file->getClientOriginalExtension();
            $fileNameResized = $fileName . '-400x230.' . $extension;
            $fileName = $fileName . '.' . $extension;
            $destinationPath = 'uploads/' . $fileNameResized;

            Image::make($file->getRealPath())->fit(400, 230)->save($destinationPath);
            //$file->move('uploads/', $fileName); // Move original?
            $shop->logo = $fileNameResized;
        }

        $shop->save();

        // redirect
        Session::flash('message', 'Empresa actualizada.');
        return Redirect::to('admin/shop');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $Shop = Shop::find($id);
        $Shop->delete();

        return redirect('admin/shop')->withFlashMessage('Empresa eliminada.');
    }

    /**
     * Clone the shop
     *
     * @return Response
     */
    public function duplicate($id)
    {
        $shop = Shop::with('services')->find($id);
        $newShop = $shop->replicate();
        $newShop->name = $shop->name . ' - copia';
        $newShop->save();
        $newShop->services()->sync($shop->services);

        return redirect('admin/shop')->withFlashMessage('Empresa duplicada.');
    }

}
