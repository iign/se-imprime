<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use App\Category;
use Illuminate\Support\Str;

use DB;

use Validator, Input, Redirect, Session;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $services = Service::with('category')
            ->orderBy('category_id', 'asc')
            ->orderBy('name', 'asc')
            ->paginate(50);

        return view('admin.services', ['services' => $services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.service_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Validate
        $rules = array(
            'service-name'      => 'required',
            'service-category'  => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/service/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $categoryID = Input::get('service-category');
            $category = Category::findOrFail($categoryID);
            $categoryName = $category->name;
            $serviceName = Input::get('service-name');

            $service = Service::where('category_id', $categoryID)->where('name', $serviceName)->get();

            if (!$service->isEmpty()) {
                Session::flash('message', "Ya existe $categoryName/$serviceName");
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to('admin/service/create')->withInput();
            }

            // store
            $service = new Service();
            $service->name = $serviceName;

            $service = $category->services()->save($service);

            // redirect
            Session::flash('message', 'Servicio creado con éxito.');
            return Redirect::to('admin/service');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.service_edit', array('service' => $service));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $service = Service::findOrFail($id);

        // Validate
        $rules = array(
            'service-name'      => 'required',
            'service-category'  => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/service/create')
                ->withErrors($validator)
                ->withInput();
        } else {

            $categoryID = Input::get('service-category');
            $category = Category::find($categoryID);

            // store
            $service->name = Input::get('service-name');
            $service = $category->services()->save($service);

            // redirect
            Session::flash('message', 'Servicio creado con éxito.');
            return Redirect::to('admin/service');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();

        return redirect('admin/service')->withFlashMessage('Servicio eliminado.');
    }
}
