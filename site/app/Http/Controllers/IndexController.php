<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;
use App\Service;
use DB;

class IndexController extends Controller
{
    /**
     * Display home page.
     *
     * @return Response
     */
    public function index()
    {

        $rubros = Service::where('category_id', 1)->orderby('name', 'ASC')->get();
        $tareas = Service::where('category_id', 2)->orderby('name', 'ASC')->get();
        $piezas = Service::where('category_id', 3)->orderby('name', 'ASC')->get();
        $terminaciones = Service::where('category_id', 4)->orderby('name', 'ASC')->get();
        $materiales = Service::where('category_id', 5)->orderby('name', 'ASC')->get();

        $categorias = array(
            'Rubros' => $rubros,
            'Tareas' => $tareas,
            'Piezas' => $piezas,
            'Terminaciones' => $terminaciones,
            'Materiales' => $materiales
        );

        $shops = DB::table('shops')->orderBy('created_at', 'desc')->take(5)->get();

        return view('index', array('shops' => $shops, 'cats' => $categorias));
    }

}
