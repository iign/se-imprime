<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;
use DB;

class IndexController extends Controller
{
    /**
     * Display home page.
     *
     * @return Response
     */
    public function index()
    {
        $shops = DB::table('shops')->where('featured', true)->orderBy('created_at', 'desc')->take(5)->get();

        return view('index', array('shops' => $shops));
    }

}
