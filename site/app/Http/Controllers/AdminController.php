<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;
use DB;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function index()
     {
        $shops = DB::table('shops')->orderBy('created_at', 'desc')->take(10)->get();
        $total = Shop::count();

        return view('admin.index', ['shops' => $shops, 'total' => $total]);
     }


}
