<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;
use App\Service;
use DB;

use Validator, Input;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function search()
    {
        $name = Input::get('name');
        $service = Input::get('service');
        $resultsPerPage = 15;

        $services = Service::findMany($service);

        if ($name && $service) {
            $shops = Shop::with('services')
                         ->where('name', 'LIKE', "%$name%")
                         ->whereHas('services', function($q) use ( $service ){
                             $q->where('id', $service);
                         })
                         ->orderBy('name', 'asc')
                         ->paginate($resultsPerPage);
        }
        elseif ($name) {
            $shops = Shop::with('services')
                         ->where('name', 'LIKE', "%$name%")
                         ->orderBy('name', 'asc')
                         ->paginate($resultsPerPage);
        }
        elseif ($service) {

            $shops = Shop::with('services');

            foreach ($service as $key => $s) {
                $shops->whereHas('services', function($q) use ($s){
                    $q->where('id', $s);
                })->orderBy('name', 'asc')
                ->paginate($resultsPerPage);;
            }
            $shops = $shops->orderBy('name', 'asc')->paginate($resultsPerPage);

        }
        else {
            $shops = Shop::with('services')
                        ->orderBy('name', 'asc')
                        ->paginate($resultsPerPage);

        }

        $terms = $name;
        $servicesText = '';
        foreach ($services as $key => $s) {
            $servicesText .= $s->name . ' & ';
        }
        $servicesText = rtrim($servicesText, ' & ');

        if ($name && $service) {
            $terms = "$name & $servicesText";
        }
        elseif(!$name){
            $terms = $servicesText;
        }

        $rubros = Service::where('category_id', 1)->orderby('name', 'ASC')->get();
        $tareas = Service::where('category_id', 2)->get();
        $piezas = Service::where('category_id', 3)->get();
        $terminaciones = Service::where('category_id', 4)->get();
        $materiales = Service::where('category_id', 5)->get();

        $categorias = array(
            'Rubros' => $rubros,
            'Tareas' => $tareas,
            'Piezas' => $piezas,
            'Terminaciones' => $terminaciones,
            'Materiales' => $materiales
        );

        return view('searchresults', ['results' => $shops, 'terms' => $terms, 'name' => $name, 'service' => $service, 'cats' => $categorias]);
    }

}
