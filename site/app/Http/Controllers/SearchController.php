<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;
use DB;

use Validator, Input;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function search()
    {
        $name = Input::get('name');
        $service = Input::get('service');
        $resultsPerPage = 15;

        if ($name && $service) {
            $shops = Shop::with('services')
                         ->where('name', 'LIKE', "%$name%")
                         ->orWhereHas('services', function($q) use ( $service ){
                            $q->where('name', 'LIKE', "%$service%");
                         })
                         ->orderBy('name', 'asc')
                         ->paginate($resultsPerPage);
        }
        elseif ($name) {
            $shops = Shop::with('services')
                         ->where('name', 'LIKE', "%$name%")
                         ->orderBy('name', 'asc')
                         ->paginate($resultsPerPage);
        }
        elseif ($service) {
            $shops = Shop::with('services')
                        ->whereHas('services', function($q) use ( $service ){
                            $q->where('name', 'LIKE', "%$service%");
                        })
                        ->orderBy('name', 'asc')
                        ->paginate($resultsPerPage);
        }
        else {
            $shops = Shop::with('services')
                        ->orderBy('name', 'asc')
                        ->paginate($resultsPerPage);
        }

        $terms = $name;

        if ($name && $service) {
            $terms = "$name & $service";
        }
        elseif(!$name){
            $terms = $service;
        }

        return view('searchresults', ['results' => $shops, 'terms' => $terms, 'name' => $name, 'service' => $service]);
    }

}
