<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

use Mail, Input;
use Validator, Redirect, Session;

use App\Service;
use DB;

class MailController extends Controller
{
    /**
     * Contacto
     *
     * @return Response
     */
    public function contact(Request $request)
    {

        if (Request::isMethod('post'))
        {
            // Validate
            $rules = array(
                'contact-name' => 'required',
                'contact-email' => 'required|email',
                'contact-message' => 'required',
                'contact-phone' => 'present|max:0'
            );

            $messages = array(
                'required' => 'El campo :attribute es obligatorio.',
                'email.required' => 'El campo de :attribute es requerido y debe ser un email válido.'
            );

            $friendly_names = array(
                'contact-name' => 'Nombre',
                'contact-email' => 'Email',
                'contact-message' => 'Comentario'
            );

            $validator = Validator::make(Input::all(), $rules, $messages);
            $validator->setAttributeNames($friendly_names);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('/contacto')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $mailData = [
                    'name' => Input::get('contact-name'),
                    'email' => Input::get('contact-email'),
                    'body' => Input::get('contact-message')
                ];

                Mail::send('emails.contact', $mailData, function ($m) use ($mailData) {
                    $m->from('robot@seimprime.uy', 'Se Imprime');
                    $m->replyTo($mailData['email'], $mailData['name']);
                    $m->to(env('MAIL_CONTACTO'), 'Se Imprime')->subject('Contacto · Se Imprime');
                });

                return view('emails.thanks');
            }
        }
        else {
            return view('contact');
        }

    }

    public function suggest(Request $request)
    {
        if (Request::isMethod('post'))
        {

            // Validate
            $rules = array(
                'name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'email' => 'email',
                'service' => 'required'
            );

            $messages = array(
                'required' => 'El campo :attribute es obligatorio.',
                'email.required' => 'El campo de :attribute es requerido y debe ser un email válido.'
            );

            $friendly_names = array(
                'name' => 'Nombre',
                'address' => 'Dirección',
                'phone' => 'Teléfonos',
                'email' => 'Email',
                'service' => 'Servicios'
            );

            $validator = Validator::make(Input::all(), $rules, $messages);
            $validator->setAttributeNames($friendly_names);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('/sugeri')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $servicesList = implode(', ', Input::get('service'));

                $mailData = [
                    'name' => Input::get('name'),
                    'address' => Input::get('address'),
                    'opening_hours' => Input::get('opening_hours'),
                    'phone' => Input::get('phone'),
                    'website' => Input::get('website'),
                    'email' => Input::get('email'),
                    'services' => $servicesList,
                    'type' => Input::get('type')
                ];

                Mail::send('emails.suggest', $mailData, function ($m) use ($mailData, $request) {
                    $m->from('robot@seimprime.uy', 'Se Imprime');
                    $m->to(env('MAIL_CONTACTO'), 'Se Imprime')->subject('Sugerencia · Se Imprime');
                    // Shop logo
                    if (Input::hasFile('logo')) {
                        $file = Input::file('logo');
                        $m->attach($file->getRealPath(), [
                            'as' => 'logo.' . $file->getClientOriginalExtension(),
                            'mime' => $file->getMimeType()
                        ]);
                    }
                });

                return view('emails.thanks_suggest');
            }
        }
        else {
            $rubros = Service::where('category_id', 1)->orderby('name', 'ASC')->get();
            $tareas = Service::where('category_id', 2)->orderby('name', 'ASC')->get();
            $piezas = Service::where('category_id', 3)->orderby('name', 'ASC')->get();
            $terminaciones = Service::where('category_id', 4)->orderby('name', 'ASC')->get();
            $materiales = Service::where('category_id', 5)->orderby('name', 'ASC')->get();

            $categorias = array(
                'Rubros' => $rubros,
                'Tareas' => $tareas,
                'Piezas' => $piezas,
                'Terminaciones' => $terminaciones,
                'Materiales' => $materiales
            );

            return view('suggest', array('cats' => $categorias));
        }
    }
}
