<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

use Mail, Input;
use Validator, Redirect, Session;

class MailController extends Controller
{
    /**
     * Contacto
     *
     * @return Response
     */
    public function contact(Request $request)
    {

        if (Request::isMethod('post'))
        {
            // Validate
            $rules = array(
                'contact-name' => 'required',
                'contact-email' => 'required|email',
                'contact-message' => 'required'
            );

            $messages = array(
                'required' => 'El campo :attribute es obligatorio.',
                'email.required' => 'El campo de :attribute es requerido y debe ser un email válido.'
            );

            $friendly_names = array(
                'contact-name' => 'Nombre',
                'contact-email' => 'Email',
                'contact-message' => 'Comentario'
            );

            $validator = Validator::make(Input::all(), $rules, $messages);
            $validator->setAttributeNames($friendly_names);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('/contacto')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $mailData = [
                    'name' => Input::get('contact-name'),
                    'email' => Input::get('contact-email'),
                    'body' => Input::get('contact-message')
                ];

                Mail::send('emails.contact', $mailData, function ($m) use ($mailData) {
                    $m->from('robot@seimprime.uy', 'Se Imprime');
                    $m->replyTo($mailData['email'], $mailData['name']);
                    $m->to(env('MAIL_CONTACTO'), 'Se Imprime')->subject('Contacto · Se Imprime');
                });

                return view('emails.thanks');
            }
        }
        else {
            return view('contact');
        }

    }

    public function suggest(Request $request)
    {
        if (Request::isMethod('post'))
        {
            
            // Validate
            $rules = array(
                'name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'email' => 'email',
                'services' => 'required'
            );

            $messages = array(
                'required' => 'El campo :attribute es obligatorio.',
                'email.required' => 'El campo de :attribute es requerido y debe ser un email válido.'
            );

            $friendly_names = array(
                'name' => 'Nombre',
                'address' => 'Dirección',
                'phone' => 'Teléfonos',
                'email' => 'Email',
                'services' => 'Servicios'
            );

            $validator = Validator::make(Input::all(), $rules, $messages);
            $validator->setAttributeNames($friendly_names);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('/sugeri')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $mailData = [
                    'name' => Input::get('name'),
                    'address' => Input::get('address'),
                    'opening_hours' => Input::get('opening_hours'),
                    'phone' => Input::get('phone'),
                    'website' => Input::get('website'),
                    'email' => Input::get('email'),
                    'services' => Input::get('services'),
                    'type' => Input::get('type')
                ];

                Mail::send('emails.suggest', $mailData, function ($m) use ($mailData, $request) {
                    $m->from('robot@seimprime.uy', 'Se Imprime');
                    $m->to(env('MAIL_CONTACTO'), 'Se Imprime')->subject('Sugerencia · Se Imprime');
                    // Shop logo
                    if (Input::hasFile('logo')) {
                        $file = Input::file('logo');
                        $m->attach($file->getRealPath(), [
                            'as' => 'logo.' . $file->getClientOriginalExtension(),
                            'mime' => $file->getMimeType()
                        ]);
                    }
                });

                return view('emails.thanks_suggest');
            }
        }
        else {
            return view('suggest');
        }
    }
}
