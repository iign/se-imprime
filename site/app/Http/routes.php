<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'IndexController@index'));
Route::get('/listado', array('as' => 'search', 'uses' => 'SearchController@search'));

Route::get('/contacto', array('as' => 'contacto', 'uses' => 'MailController@contact'));
Route::post('/contacto', array('uses' => 'MailController@contact'));

// Sugerir comercio
Route::get('/sugeri', array('as' => 'sugeri', 'uses' => 'MailController@suggest'));
Route::post('/sugeri', array('uses' => 'MailController@suggest'));

Route::get('empresa/{id}/{slug}', array('as' => 'shop.view', 'uses' => 'ShopController@show'));

Route::get('/acerca', function()
{
    return View::make('acerca');
});

// Rutas de Admin
Route::group(['middleware' => 'auth'], function () {

    Route::get('admin', 'AdminController@index');
    Route::resource('admin/category', 'CategoryController');
    Route::resource('admin/service', 'ServiceController');

    // Shops
    Route::get('admin/shop/{id}/duplicate', array('as' => 'admin.shop.duplicate', 'uses' => 'ShopController@duplicate'));
    Route::resource('admin/shop', 'ShopController');

});


// Rutas de Autentificación
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
