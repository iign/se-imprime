<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';
    public $timestamps = false;

    /**
     * Gets the category this service belongs to.
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Gets all the shops that provide this service.
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function shops()
    {
        // TODO
    }
}
