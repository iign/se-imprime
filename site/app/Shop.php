<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{

    /**
    * Get the services provided by this shop.
    */
    public function services()
    {
      return $this->belongsToMany('App\Service', 'service_shop');
    }


}
