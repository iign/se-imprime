## Development

$ ssh homestead
$ cd Projects/seimprime/site
$ gulp watch
$ open seimprime.dev

## Deploy

$ cd Projects/seimprime/site
$ envoy run deploy-beta

### Migrations
$ ssh montag && cd webapps/seimprime_beta
$ php70 artisan migrate
