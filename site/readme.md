## Development

$ ssh homestead
$ cd Projects/seimprime/site
$ gulp watch
$ open seimprime.dev

## Deploy

$ cd Projects/seimprime/site
$ envoy run deploy-beta

### Migrations
$ ssh montag && cd webapps/seimprime_beta
$ php70 artisan migrate

##  Run Tests

From VM run:
$ cd Projects/seimprime/site
$ ./vendor/bin/phpunit
