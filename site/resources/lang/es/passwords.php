<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Contraseña debe tener al menos seis caracteres y coincidir con la confirmación.',
    'reset' => 'Tu contraseña ha sido reseteada.',
    'sent' => 'Te enviamos un link para resetear tu contraseña.',
    'token' => 'Token inválido.',
    'user' => "No existe usuario con ese e-mail",

];
