@extends('layouts.master')

@section('title', $shop->name)
@section('description', $shop->name . ' - Guía de proveedores gráficos Uruguay.')

@section('fb-image', route('home') . '/uploads/' . $shop->logo)

@section('content')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq0yrE4g6RoilDVLAVa9QOBZIUg1vxCvM&callback=initMap"
        async defer></script>
<script>
    SI.shop = {}
    SI.shop.lat = {{ $shop->lat }}
    SI.shop.lng = {{ $shop->lng }}

    var map

    function initMap () {
        var latLng = {lat: SI.shop.lat, lng: SI.shop.lng};
        map = new google.maps.Map(document.getElementById('map'), {
          center: latLng,
          zoom: 16
        })
        var marker = new google.maps.Marker({
          position: latLng,
          map: map,
          title: '{{$shop->name}}'
        })
    }

</script>

<div class="content-wrap">
    <section class="main-content">
        <div class="shop">
            <div class="info">
              <div class="div logo">
                  @if($shop->logo)
                    <img class="shop-logo" src="/uploads/{{ $shop->logo }}" alt="{{ $shop->name }}">
                  @else
                      <img class="shop-logo" src="{{ route('home') }}/img/no-logo.png" alt="{{ $shop->name }}">
                  @endif

              </div>
              <div class="detail">
                <h1 class="name">{{ $shop->name }}</h1>
                <p class="data address">{{ $shop->address }}</p>
                <p class="data time">{{ $shop->opening_hours }}</p>
                <p class="data tel">{{ $shop->phone }}</p>
                <p class="data url"><a href="{{ $shop->website }}">{{ $shop->website }}</a></p>
                <p class="data email">
                    <a href="mailto:{{ $shop->email }}">{{ $shop->email }}</a>
                </p>
              </div>

            </div>

            <div class="map-mini js-show-map">
                <img border="0" src="/img/map-mini.jpg">
            </div>

            <div class="shop-details">

                <div class="shop-col">

                    <div class="shop-widget">
                      <h3 class="shop-widget__title">Rubro</h3>
                      <ul class="shop-widget__list">
                          @foreach ($shop->services as $s)
                            @if($s->category_id == 1)
                                <li>{{$s->name}}</li>
                            @endif
                          @endforeach
                      </ul>
                    </div>

                    <div class="shop-widget">
                      <h3 class="shop-widget__title">Tipo de trabajo</h3>
                      <ul class="shop-widget__list">
                          @foreach ($shop->services as $s)
                            @if($s->category_id == 2)
                                <li>{{$s->name}}</li>
                            @endif
                          @endforeach
                      </ul>
                    </div>
                </div>

                <div class="shop-col">
                    <div class="shop-widget">
                      <h3 class="shop-widget__title">Tipo de pieza</h3>
                      <ul class="shop-widget__list">
                          @foreach ($shop->services as $s)
                            @if($s->category_id == 4)
                                <li>{{$s->name}}</li>
                            @endif
                          @endforeach
                      </ul>
                    </div>

                    <div class="shop-widget">
                      <h3 class="shop-widget__title">Terminaciones</h3>
                      <ul class="shop-widget__list">
                          @foreach ($shop->services as $s)
                            @if($s->category_id == 3)
                                <li>{{$s->name}}</li>
                            @endif
                          @endforeach

                      </ul>
                    </div>
                </div>

                <div class="shop-col">
                    <div class="shop-widget">
                      <h3 class="shop-widget__title">Materiales</h3>
                      <ul class="shop-widget__list">
                          @foreach ($shop->services as $s)
                            @if($s->category_id == 5)
                                <li>{{$s->name}}</li>
                            @endif
                          @endforeach

                      </ul>
                    </div>
                </div>

            </div>

            <div class="comment-section">
                <div class="title">Comentá</div>

                <div class="disqus-wrap">
                    <div id="disqus_thread"></div>
                    <script>
                        var disqus_config = function () {
                            this.page.url = '{{Request::url()}}';
                            this.page.identifier = '{{$shop->id}}' + '-' +  '{{ $shop->slug }}';
                        };
                    </script>
                    @if(env('APP_ENV') == 'local')
                        <script>
                            (function() {
                                var d = document, s = d.createElement('script');
                                s.src = '//seimprimetesting.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                    @elseif(env('APP_ENV') == 'beta')
                        <script>
                            (function() {
                                var d = document, s = d.createElement('script');
                                s.src = '//seimprimebeta.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                    @else
                        <script>
                            (function() {
                                var d = document, s = d.createElement('script');
                                s.src = '//seimprime.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                    @endif
                        <noscript>Active JavaScript para leer los comentarios.</noscript>
                </div>

            </div>

        </div> <!-- end shop -->
    </section>

</div>

<div class="shop-map-modal js-shop-map-modal">
    <div id="map" class="js-shop-map shop-map"></div>
    <button type="button" class="js-close-map btn-close-map">&times;</button>
</div>

@endsection
