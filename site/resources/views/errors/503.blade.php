@extends('layouts.master')

@section('title', 'Error')

@section('body-class'){{ 'error-503' }}@stop

@section('content')

    <div class="content-wrap">
    <section class="main-content">

        <div class="main-container container">

            <h1>Error 503</h1>

            <p>
                Uups! Estarmos de vuelta en breve. <br>
                Disculpa las molestias.
            </p>


        </div> <!-- end left -->
    </section>

</div>


@endsection
