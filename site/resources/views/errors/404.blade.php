@extends('layouts.master')

@section('title', '404')

@section('body-class'){{ '404' }}@stop

@section('content')

    <div class="content-wrap">
    <section class="main-content">

        <div class="main-container container">

            <div class="container-404">
                <img class="img-404" src="img/404.svg" alt="Página no encontrada." />
                <h1 class="title-404">
                    No hemos encontrado <br>lo que buscas, lo sentimos.
                </h1>
            </div>


        </div> <!-- end left -->
    </section>

</div>


@endsection
