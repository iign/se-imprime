<hr class="separator">

<footer class="footer">
  <div class="left">
    <span class="site-logo-small">SE IMPRIME</span>
    <span class="copy">&copy; - Todos los derechos reservados.</span>
  </div>
  <div class="right pages">
    <nav class="site-nav">
        <ul class="inline-block-list">
            <li><a class="nav-item" href="{{ route('search') }}">Listado de comercios</a></li>
            <li><a class="nav-item" href="{{ route('sugeri') }}">Sugerí</a></li>
            <li><a class="nav-item" href="/acerca">Acerca De</a></li>
            <li><a class="nav-item" href="{{ route('contacto') }}">Contacto</a></li>
        </ul>
    </nav>
  </div>
</footer>
