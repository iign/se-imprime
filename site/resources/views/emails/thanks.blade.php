@extends('layouts.master')

@section('title', 'Gracias por contactarnos')

@section('body-class'){{ 'thank-you' }}@stop

@section('content')


    <div class="content-wrap">
    <section class="main-content">

        <div class="main-container container">

            <h1>¡Gracias!</h1>
            <p>
                Nos contactaremos a la brevedad.
            </p>

        </div> <!-- end left -->
    </section>

</div>


@endsection
