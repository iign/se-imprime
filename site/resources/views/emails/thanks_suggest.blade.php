@extends('layouts.master')

@section('title', 'Gracias por tu sugerencia')

@section('body-class'){{ 'thank-you' }}@stop

@section('content')


    <div class="content-wrap">
    <section class="main-content">

        <div class="main-container container">

            <h1>¡Gracias!</h1>
            <p>
                A la brevedad revisaremos la solicitud y actualizaremos la base de datos.
            </p>

        </div> <!-- end left -->
    </section>

</div>


@endsection
