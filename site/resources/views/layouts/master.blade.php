<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@yield('title', 'Se Imprime · Guía de proveedores gráficos Uruguay')</title>
        <meta name="description" content="@yield('description', 'Guía de proveedores gráficos Uruguay')">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js"></script>
        <script>
          WebFont.load({
            google: {
              families: ['Lato:300,400,700']
            }
          });
        </script>

        @include('_favicons')

        <meta property="og:image" content="@yield('fb-image', route('home') . '/fb.jpg')" />
        <meta property="og:title" content="Se Imprime"/>
        <meta property="og:site_name" content="Se Imprime | Guía de proveedores gráficos Uruguay"/>

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
        <script>var SI = {}</script>
    </head>
    <body class="page-@yield('body-class', 'default')">

        @if(env('APP_ENV') == 'beta')
            <div class="beta-notice">
                Se Imprime - βeta
            </div>
        @endif

        <main id="main-wrap" class="main-wrap" role="main">

          <header class="site-header">
              <a href="/" class="site-logo" rel="home">
                  <h1>Se Imprime</h1>
              </a>

              <nav class="site-nav">
                  <ul class="inline-block-list">
                      <li><a class="nav-item {{ Request::path() == '/' ? 'selected' : '' }}" href="/">Inicio</a></li>
                      <li><a class="nav-item {{ Request::path() == 'listado' ? 'selected' : '' }}" href="{{ route('search') }}">Listado de comercios</a></li>
                      <li><a class="nav-item {{ Request::path() == 'sugeri' ? 'selected' : '' }}" href="{{ route('sugeri') }}">Sugerí</a></li>
                      <li><a class="nav-item {{ Request::path() == 'contacto' ? 'selected' : '' }}" href="{{ route('contacto') }}">Contacto</a></li>
                  </ul>
              </nav>
          </header>

          @yield('content')

          {{-- @include('banners') --}}

          @include('footer')

        </main>

        <script src="{{ asset('js/all.js') }}"></script>
        @include('_analytics')
        @yield('scripts')

    </body>
</html>
