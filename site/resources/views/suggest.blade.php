@extends('layouts.master')

@section('title', 'Sugerí un comercio')

@section('body-class'){{ 'suggest' }}@stop

@section('content')

    <div class="content-wrap">
    <section class="main-content">

        <div class="main-container container">

            <h1>Sugerí o editá un comercio</h1>

            {!! BootForm::open(array('action' => 'MailController@suggest'))->multipart()->class('form-contact form') !!}

                <div class="form-errors">
                    {!! HTML::ul($errors->all()) !!}
                </div>

                <div class="form-row">
                    <div class="form-item">
                        {!! BootForm::select('Seleccione una opción', 'type')
                            ->options([
                                'Sugerir un nuevo comercio' => 'Sugerir un nuevo comercio',
                                'Editar un comercio existente' => 'Editar un comercio existente'])
                            ->select('green')
                            ->addClass('select-yellow')->required() !!}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item form-item--half">
                        {!! BootForm::text('Nombre del comercio*', 'name')->required() !!}
                    </div>
                    <div class="form-item form-item--half">
                        {!! BootForm::text('Dirección*', 'address')->required() !!}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item form-item--half">
                        {!! BootForm::text('Horario', 'opening_hours') !!}
                    </div>
                    <div class="form-item form-item--half">
                        {!! BootForm::text('Teléfonos*', 'phone')->required() !!}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item form-item--half">
                        {!! BootForm::text('Sitio web', 'website') !!}
                    </div>
                    <div class="form-item form-item--half">
                        {!! BootForm::email('E-mail', 'email') !!}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item form-item-logo">
                        <label class="control-label" for="droppable">Logo</label>
                        <div class="droppable" id="droppable">
                            <div class="data">
                                <div class="hint">Arrastrá y soltá tu logo aquí. (Formato PNG o JGP, máximo 1MB)</div>
                            </div>
                            <div class="joya hidden">¡Joya!</div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item">
                        <label for="services">Servicios
                            <span class="lowercase">(Rubro, Tipo de trabajo, Terminaciones, Tipo de pieza y Materiales.)*</span>
                        </label>
                        <select multiple class="form-control suggest-search si-select2" name="service[]" style="width: 100%">
                            <optgroup label="Rubros">
                                @foreach($cats['Rubros'] as $key => $cat)
                                    <option value="{{$cat->name}}">{{$cat->name}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Tareas">
                                @foreach($cats['Tareas'] as $key => $cat)
                                    <option value="{{$cat->name}}">{{$cat->name}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Tareas">
                                @foreach($cats['Tareas'] as $key => $cat)
                                    <option value="{{$cat->name}}">{{$cat->name}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Piezas">
                                @foreach($cats['Piezas'] as $key => $cat)
                                    <option value="{{$cat->name}}">{{$cat->name}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Terminaciones">
                                @foreach($cats['Terminaciones'] as $key => $cat)
                                    <option value="{{$cat->name}}">{{$cat->name}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Materiales">
                                @foreach($cats['Materiales'] as $key => $cat)
                                    <option value="{{$cat->name}}">{{$cat->name}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item">
                        <input type="submit" value="enviar" class="do-search btn-send btn-yellow-rounded btn">
                    </div>
                </div>

            {!! BootForm::close() !!}

        </div> <!-- end left -->
    </section>

</div>


@endsection
