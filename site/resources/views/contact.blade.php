@extends('layouts.master')

@section('title', 'Contacto')

@section('body-class'){{ 'contact' }}@stop

@section('content')

    <div class="content-wrap">
    <section class="main-content">

        <div class="main-container container">

            <h1>Contáctanos</h1>

            <p>
                Comuniquese con nosotros por cualquier consulta o sugerencia y le responderemos a la brevedad. <br>
                Si desea sugerir un nuevo comercio o hacer cambios, ingrese <a href="/sugeri">aquí</a> y complete el formulario.
            </p>

            {!! BootForm::open(array('action' => 'MailController@contact'))->class('form-contact form') !!}

                <div class="form-errors">
                    {!! HTML::ul($errors->all()) !!}
                </div>

                <div class="form-row">
                    <div class="form-item form-item--half">
                        {!! BootForm::text('Nombre', 'contact-name')->required() !!}
                    </div>
                    <div class="form-item form-item--half">
                        {!! BootForm::email('Email', 'contact-email')->required() !!}
                    </div>
                </div>

                <div class="form-row" style="display: none; height: 0; pointer-events: none;">
                    <div class="form-item">
                        <input type="text" name="contact-phone">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item">
                        {!! BootForm::textarea('Comentario', 'contact-message')->required() !!}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-item">
                        <input type="submit" value="enviar" class="do-search btn-send btn-yellow-rounded btn">
                    </div>
                </div>

            {!! BootForm::close() !!}

        </div> <!-- end left -->
    </section>

</div>


@endsection
