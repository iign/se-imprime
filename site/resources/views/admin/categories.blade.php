@extends('admin/app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Categorías de servicios</div>

                <div class="panel-body">

                  @if (Session::has('message'))
                      <div class="alert alert-info">{{ Session::get('message') }}</div>
                  @endif

                  <ul>
                    @foreach ($categories as $cat)
                      <li>
                        <a href="{{ URL::route('admin.category.edit', $cat->id) }}"
                           class="btn btn-link">
                          {{ $cat->name }}
                        </a>
                      </li>
                    @endforeach
                  </ul>
                    <hr>
                  <div>
                      <a href="{{ URL::route('admin.category.create') }}"
                         class="btn btn-default" role="button">
                        Crear Categoría
                      </a>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
