@extends('admin/app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Servicio ({!! $service->name !!})</div>

                    <div class="panel-body">

                        {!! HTML::ul($errors->all()) !!}

                        {!! BootForm::open()->put()->action(route('admin.service.update', ['id' => $service->id])) !!}

                        <div class="form-group {{ $errors->has('service-name') ? ' has-error' : '' }}">
                            {!! BootForm::text('Nombre', 'service-name')->required()->defaultValue($service->name) !!}
                        </div>

                        <div class="form-group {{ $errors->has('service-category') ? ' has-error' : '' }}">
                            {!! BootForm::select('Categoría', 'service-category')
                                ->options(['1' => 'Rubro', '2' => 'Tarea', '3' => 'Pieza', '4' => 'Terminaciones', '5' => 'Material'])
                                ->select($service->category_id) !!}
                        </div>

                        {!! BootForm::submit('Guardar')->class('btn btn-primary') !!}
                        {!! BootForm::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
