@extends('admin/app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Empresa ({!! $shop->name !!})</div>

                    <div class="panel-body">

                        {!! HTML::ul($errors->all()) !!}

                        {!! BootForm::open()->multipart()->put()->action(route('admin.shop.update', ['id' => $shop->id])) !!}

                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! BootForm::text('Nombre', 'shop-name')->required()->defaultValue($shop->name) !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('address') ? ' has-error' : '' }}">
                                {!! BootForm::text('Dirección', 'shop-address')->required()->defaultValue($shop->address) !!}
                            </div>
                            <div class="form-group form-group col-md-6 {{ $errors->has('opening_hours') ? ' has-error' : '' }}">
                                {!! BootForm::text('Horario', 'shop-opening_hours')->defaultValue($shop->opening_hours) !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('phone') ? ' has-error' : '' }}">
                                {!! BootForm::text('Teléfono', 'shop-phone')->required()->defaultValue($shop->phone) !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('website') ? ' has-error' : '' }}">
                                {!! BootForm::text('Sitio Web', 'shop-website')->defaultValue($shop->website) !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! BootForm::email('E-mail', 'shop-email')->defaultValue($shop->email) !!}
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
                                    {!! BootForm::file('Logo', 'shop-logo') !!}
                                </div>

                                @if($shop->logo)
                                    <div class="form-group">
                                        <label class="control-label" for="shop-logo-current">Logo Actual</label>
                                        <div class="current-logo-wrap">
                                            <img class="current-logo-img" src="/uploads/{{ $shop->logo }}" alt="Loco Actual" />
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="shop-logo-current">Ubicación</label>
                                    <div class="map-select">
                                        <div class="map"></div>
                                        <div class="row form-group">
                                            <div class="col-xs-9">
                                                <input type="text" name="search-address" class="search-address form-control" placeholder="Buscar dirección" value="">
                                            </div>
                                            <div class="col-xs-3">
                                                <button type="button" class="btn btn-secondary btn-block js-search-address">Buscar</button>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                {!! BootForm::text('Latitud', 'shop-lat')->addClass('js-shop-lat')->defaultValue($shop->lat) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! BootForm::text('Latitud', 'shop-lng')->addClass('js-shop-lng')->defaultValue($shop->lng) !!}
                                                {{-- <input type="text" class="form-control js-shop-lng" name="shop-lng" placeholder="Longitud"> --}}
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-primary js-reset-location">Borrar Ubicación</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('featured') ? ' has-error' : '' }}">
                                    <label class="control-label" for="shop-featured">Opciones</label>
                                    @if ($shop->featured)
                                        {!! BootForm::checkbox('Destacado en portada', 'shop-featured')->check() !!}
                                    @else
                                        {!! BootForm::checkbox('Destacado en portada', 'shop-featured')->uncheck() !!}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
                                    <label class="control-label" for="shop-services">Servicios Asociados</label>
    
                                    <select multiple class="text-input form-control js-services" name="shopServices[]" style="width: 100%">
                                        <optgroup label="Rubros">
                                            @foreach($services['Rubros'] as $key => $s)
                                                <option @if(in_array($s->id, $shopServices))selected="selected"@endif value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Tareas">
                                            @foreach($services['Tareas'] as $key => $s)
                                                <option @if(in_array($s->id, $shopServices))selected="selected"@endif value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Tareas">
                                            @foreach($services['Tareas'] as $key => $s)
                                                <option @if(in_array($s->id, $shopServices))selected="selected"@endif value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Piezas">
                                            @foreach($services['Piezas'] as $key => $s)
                                                <option @if(in_array($s->id, $shopServices))selected="selected"@endif value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Terminaciones">
                                            @foreach($services['Terminaciones'] as $key => $s)
                                                <option @if(in_array($s->id, $shopServices))selected="selected"@endif value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Materiales">
                                            @foreach($services['Materiales'] as $key => $s)
                                                <option @if(in_array($s->id, $shopServices))selected="selected"@endif value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>

                        {!! BootForm::submit('Guardar')->class('btn btn-primary') !!}
                        {!! BootForm::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
