@extends('admin/app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Añadir Empresa</div>

                    <div class="panel-body">

                        {!! HTML::ul($errors->all()) !!}

                        {!! BootForm::open()->multipart()->post()->action(route('admin.shop.store')) !!}

                        <div class="row">
                            <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! BootForm::text('Nombre', 'shop-name')->required() !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('address') ? ' has-error' : '' }}">
                                {!! BootForm::text('Dirección', 'shop-address')->required() !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('opening_hours') ? ' has-error' : '' }}">
                                {!! BootForm::text('Horario', 'shop-opening_hours') !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('phone') ? ' has-error' : '' }}">
                                {!! BootForm::text('Teléfono', 'shop-phone')->required() !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('website') ? ' has-error' : '' }}">
                                {!! BootForm::text('Sitio Web', 'shop-website') !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! BootForm::email('E-mail', 'shop-email') !!}
                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('logo') ? ' has-error' : '' }}">
                                {!! BootForm::file('Logo', 'shop-logo') !!}
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="shop-logo-current">Ubicación</label>
                                    <div class="map-select">
                                        <div class="map"></div>
                                        <div class="row form-group">
                                            <div class="col-xs-9">
                                                <input type="text" name="search-address" class="search-address form-control" placeholder="Buscar dirección" value="">
                                            </div>
                                            <div class="col-xs-3">
                                                <button type="button" class="btn btn-secondary btn-block js-search-address">Buscar</button>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control js-shop-lat" name="shop-lat" placeholder="Latitud">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control js-shop-lng" name="shop-lng" placeholder="Longitud">
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-primary js-reset-location">Borrar Ubicación</button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('featured') ? ' has-error' : '' }}">
                                <label class="control-label" for="shop-featured">Opciones</label>
                                {!! BootForm::checkbox('Destacado en portada', 'shop-featured') !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
                                    <label class="control-label" for="shop-services">Servicios Asociados</label>
                                    <ul class="unstyled-list">
                                        @foreach($services as $s)
                                            <li>
                                                {!! Form::checkbox('shopServices[]', $s->id, in_array($s->id, $shopServices), array('id' => 'services_' . $s->id)) !!}
                                                {!! Form::label('services_' . $s->id, $s->name) !!}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! BootForm::submit('Guardar')->class('btn btn-primary') !!}
                            </div>
                        </div>
                        {!! BootForm::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
