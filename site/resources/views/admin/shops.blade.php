@extends('admin/app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('flash_message'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Empresas (Total: <?php echo $shops->total() ?>)</div>

                <div class="panel-body">
                  {!! BootForm::open()->get()->action('/admin/shop'); !!}
                    <div class="form-group">
                      <input type="text" name="q" class="form-control" placeholder="Buscar">
                    </div>
                  {!! BootForm::close() !!}

                  <div class="table-responsive">
                    <table class="table table-striped table-shops">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Logo</th>
                          <th>Nombre</th>
                          <th>Dirección</th>
                          <th>Horario</th>
                          <th>Teléfono</th>
                          <th>E-mail</th>
                          <th>Destacado?</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>

                      <tbody>
                      @foreach ($shops as $s)
                      <tr>
                          <th scope="row">{{ $s->id }}</th>
                          <td>
                            @if ($s->logo)
                              <a href="{{ route('admin.shop.edit', $s->id) }}">
                                <img class="current-logo-img table-thumb" src="/uploads/{{ $s->logo }}" alt="Logo" />
                              </a>
                            @endif
                          </td>
                          <td>{{ $s->name }}</td>
                          <td>{{ $s->address }}</td>
                          <td>{{ $s->opening_hours }}</td>
                          <td>{{ $s->phone }}</td>
                          <td>{{ $s->email }}</td>
                          <td>
                              @if ($s->featured)
                              ✅
                              @endif
                          </td>
                          <td class="td-actions">

                            <a title="Editar" class="btn btn-default btn-sm" href="{{ route('admin.shop.edit', $s->id) }}">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>

                            <a title="Clonar" class="btn btn-default btn-sm" href="{{ route('admin.shop.duplicate', $s->id) }}">
                                <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
                            </a>

                            <a title="Ver" target="_" class="btn btn-default btn-sm" href="{{ route('shop.view', [$s->id, $s->slug]) }}">
                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                            </a>

                            {!! BootForm::open()->delete()->class('delete-form js-delete-shop-form')->action(route('admin.shop.destroy',
                                ['id' => $s->id])) !!}
                                <button type="submit" class="btn btn-danger btn-sm" name="button">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            {!! BootForm::close() !!}
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
            <?php echo $shops->appends(['q' => $query ])->render(); ?>
        </div>
    </div>
</div>
@endsection
