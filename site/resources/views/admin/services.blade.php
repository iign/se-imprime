@extends('admin/app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('flash_message'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Servicios (Total: <?php echo $services->total() ?>)</div>

                <div class="panel-body">

                  <div class="table-responsive">
                    <table class="table table-striped table-shops">
                      <thead>
                        <tr>
                          <th>Categoría</th>
                          <th>Nombre</th>
                          <th>Acción</th>
                        </tr>
                      </thead>

                      <tbody>
                      @foreach ($services as $s)
                      <tr>
                          <td scope="row">{{ $s->category->name }}</td>
                          <td>{{ $s->name }}</td>
                          <td class="td-actions">

                            <a class="btn btn-default btn-sm" href="{{ route('admin.service.edit', $s->id) }}">&#9998; Editar</a>

                            {!! BootForm::open()->delete()->class('delete-form js-delete-shop-form')->action(route('admin.service.destroy',
                                ['id' => $s->id])) !!}
                              <input class="btn btn-danger btn-sm" type="submit" value="&times;" />
                            {!! BootForm::close() !!}
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>

                </div>
            </div>
            {!! $services->render() !!}
        </div>
    </div>
</div>
@endsection
