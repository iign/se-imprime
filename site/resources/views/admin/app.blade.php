<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Se Imprime</title>

    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq0yrE4g6RoilDVLAVa9QOBZIUg1vxCvM"></script>
    <script>var SI = {}</script>
</head>
<body>
    @if (Auth::check())
        <nav class="navbar navbar-default">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Menú</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/admin') }}">Admin / Se Imprime</a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                        role="button" aria-haspopup="true" aria-expanded="false">
                            Servicios <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li><a href="/admin/service">Ver Todos</a></li>
                          <li><a href="/admin/service/create">Añadir Nuevo</a></li>
                        </ul>
                    </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                    role="button" aria-haspopup="true" aria-expanded="false">
                        Empresas <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li><a href="/admin/shop">Ver Todas</a></li>
                      <li><a href="/admin/shop/create">Añadir Nueva</a></li>
                    </ul>
                  </li>
                  <li>
                    <a target="blank" href="/">
                      Ver sitio <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
                    </a>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                @if (Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                         role="button" aria-expanded="false">
                         {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                      <ul class="dropdown-menu" role="menu">
                          <li><a href="{{ url('/auth/logout') }}">Salir</a></li>
                      </ul>
                  </li>
                @endif
                </ul>
              </div>

            </div>
        </nav>
    @endif

    @yield('content')

    <script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>
