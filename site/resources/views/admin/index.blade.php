@extends('admin/app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Administración</div>

                <div class="panel-body">
                    <h2>Últimas empresas creadas</h2>

                    @if (count($shops) > 0)
                    <ul>
                        @foreach ($shops as $s)
                        <li>
                          <a href="{{ route('admin.shop.edit', $s->id) }}">{{ $s->name }}</a>
                          <small>{{ date("d/m/Y H:i", strtotime( $s->created_at )) }}</small>
                        </li>
                        @endforeach
                    </ul>
                    @else
                    <p>
                      Aún no hay contenido creado...
                    </p>
                    <p>
                      <a class="btn btn-default" href="/admin/shop/create"
                         role="button">Crear Empresas</a>
                    </p>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
