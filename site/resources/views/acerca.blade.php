@extends('layouts.master')

@section('title', 'Acerca de')

@section('body-class'){{ 'contact' }}@stop

@section('content')

<div class="content-wrap">
    <section class="main-content">

        <div class="main-container container default-page container--padded">

            <h1>Acerca De</h1>

            <p>
                <em>Se Imprime</em> es una herramienta colaborativa que busca
                generar un listado de proveedores gráficos.
            </p>

            <h2>Comentarios</h2>

            <p>
                <em>Se Imprime</em> no se hace responsable de los comentarios
                realizados por los usuarios.<br>
                Serán moderados para mantener la paz y el respeto.
            </p>

            <h2>Comercios</h2>

            <p>
                La información de los comercios listados en esta primera etapa fue
                recabada por el equipo de <em>Se Imprime</em>. Si desean cambiar o ser
                quitados del listado podrán enviar un mail a <a href="mailto:sugeri@seimprime.uy">sugeri@seimprime.uy</a>
            </p>

        </div> <!-- end left -->
    </section>

</div>


@endsection
