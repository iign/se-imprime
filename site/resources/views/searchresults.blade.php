@extends('layouts.master')

@section('title', 'Listado de comercios')
@section('description', 'Listado de comercios')

@section('body-class'){{ 'search-results' }}@stop

@section('content')

    {!! Form::open(array('route' => 'search', 'method' => 'get', 'class' => 'home-search')) !!}

        <div class="by-name col">
            <h4 class="title">Buscar por nombre de local</h4>
            <input name="name" placeholder="Ej.: Imprenta Rojo, Copiplan" type="text" class="text-input"
                   pattern=".{0}|.{3,}" title="Ingrese al menos 3 caracteres" />
        </div>

        <h4 class="title or">o</h4>

        <div class="by-type col">
            <h4 class="title">Buscar por tipo de trabajo o pieza</h4>
            <input name="service" placeholder="Ej.: Ploteo, Volante, Serigrafía" type="text" class="text-input"
                   pattern=".{0}|.{2,}" title="Ingrese al menos 3 caracteres" />
        </div>

        <input type="submit" value="Buscar" class="do-search btn-green btn">

    {!! Form::close() !!}

    @if($terms)
        <div class="query-header">
            Resultados de la búsqueda <span class="query-header__term">&ldquo;{{ $terms }}&rdquo;</span>
        </div>
    @endif

    <div class="content-wrap">
    <section class="main-content">

        <div class="main-container">

            <ul class="result-list ranking">
                @forelse ($results as $s)
                    <li class="shop-strip">
                      <a href="{{ route('shop.view', [$s->id, $s->slug]) }}" class="info">
                        <h3 class="name">{{ $s->name }}</h3>
                        <div class="data">
                          {{ $s->address }} - {{ $s->phone }}
                        </div>
                      </a>
                    </li>
                @empty
                    <p>
                        No se encontraron resultados :(
                    </p>
                @endforelse
            </ul>

            {!! $results->appends(['name' => $name, 'service' => $service])->render() !!}

        </div> <!-- end left -->
    </section>


</div>


@endsection
