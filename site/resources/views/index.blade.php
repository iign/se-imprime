@extends('layouts.master')

@section('content')

  {!! Form::open(array('route' => 'search', 'method' => 'get', 'class' => 'home-search')) !!}

      <div class="by-name col">
          <h4 class="title">Buscar por nombre de local</h4>
          <input name="name" placeholder="Ej.: Imprenta Rojo, Copiplan" type="text" class="text-input"
                 pattern=".{0}|.{3,}" title="Ingrese al menos 3 caracteres" />
      </div>

      <h4 class="title or">o</h4>

      <div class="by-type col">
          <h4 class="title">Buscar por tipo de trabajo o pieza</h4>
          <input name="service" placeholder="Ej.: Ploteo, Volante, Serigrafía" type="text" class="text-input"
                 pattern=".{0}|.{2,}" title="Ingrese al menos 3 caracteres" />
      </div>

      <input type="submit" value="Buscar" class="do-search btn-green btn">

  {!! Form::close() !!}

  <div class="home-workflow">
      <div class="col busca"><span class="item">Buscá</span></div>
      <div class="col arrow"></div>
      <div class="col subraya"><span class="item">Subrayá</span></div>
      <div class="col arrow"></div>
      <div class="col comunicate"><span class="item">Comunicate</span></div>
      <div class="col arrow"></div>
      <div class="col presupuesta"><span class="item">Presupuestá</span></div>
  </div>

  <section class="lists">
    <div class="full-col ranking">
      <h2 class="list__title">Empresas Recientes</h2>
      <ul>
        @foreach ($shops as $s)
            <li class="shop-strip">
              <a href="{{ route('shop.view', [$s->id, $s->slug]) }}" class="info">
                <h3 class="name">{{ $s->name }}</h3>
                <div class="data">
                  {{ $s->address }} - {{ $s->phone }}
                </div>
              </a>
            </li>
        @endforeach
      </ul>
    </div>

  </section>
@endsection
