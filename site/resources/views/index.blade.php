@extends('layouts.master')

@section('body-class'){{ 'home' }}@stop
@section('content')

  {!! Form::open(array('route' => 'search', 'method' => 'get', 'class' => 'home-search')) !!}

      <div class="by-name col">
          <h4 class="title">Buscar por nombre de local</h4>
          <input name="name" placeholder="Ej.: Imprenta Rojo, Copiplan" type="text" class="text-input"
                 pattern=".{0}|.{3,}" title="Ingrese al menos 3 caracteres" />
      </div>

      <h4 class="title or">o</h4>

      <div class="by-type col">
          <h4 class="title">Buscar por tipo de trabajo o pieza</h4>
          <select multiple class="text-input si-search si-select2" name="service[]" style="width: 100%">
              <optgroup label="Rubros">
                  @foreach($cats['Rubros'] as $key => $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
              </optgroup>
              <optgroup label="Tareas">
                  @foreach($cats['Tareas'] as $key => $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
              </optgroup>
              <optgroup label="Tareas">
                  @foreach($cats['Tareas'] as $key => $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
              </optgroup>
              <optgroup label="Piezas">
                  @foreach($cats['Piezas'] as $key => $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
              </optgroup>
              <optgroup label="Terminaciones">
                  @foreach($cats['Terminaciones'] as $key => $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
              </optgroup>
              <optgroup label="Materiales">
                  @foreach($cats['Materiales'] as $key => $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
              </optgroup>
          </select>
      </div>

      <input type="submit" value="Buscar" class="do-search btn-green btn">

  {!! Form::close() !!}

  <section class="lists">
    <div class="full-col ranking">
      <h2 class="list__title">Empresas Recientes</h2>
      <ul>
        @foreach ($shops as $s)
            <li class="shop-strip">
              <a href="{{ route('shop.view', [$s->id, $s->slug]) }}" class="info">
                <h3 class="name">{{ $s->name }}</h3>
                <div class="data">
                  {{ $s->address }} - {{ $s->phone }}
                </div>
              </a>
            </li>
        @endforeach
      </ul>
    </div>

  </section>
@endsection
