/* global SI, $, GMaps */

$(function () {
  $('.js-delete-shop-form').submit(function (event) {
    return window.confirm('¿Seguro desea eliminar este ítem?')
  })

  $('.js-reset-location').on('click', function () {
    SI.map.removeMarkers()
    $('.js-shop-lat').val('')
    $('.js-shop-lng').val('')
  })

  SI.initMap()

  $('.js-search-address').on('click', function () {
    GMaps.geocode({
      address: $('.search-address').val(),
      callback: function (results, status) {
        console.log(results, status)
        if (status === 'OK') {
          SI.map.removeMarkers()
          SI.map.setZoom(16)
          var latlng = results[0].geometry.location
          SI.map.setCenter(latlng.lat(), latlng.lng())
          SI.map.addMarker({
            lat: latlng.lat(),
            lng: latlng.lng()
          })
          $('.js-shop-lat').val(latlng.lat())
          $('.js-shop-lng').val(latlng.lng())
        } else {
          console.log(results)
        }
      }
    })
  })

  $('.js-services').select2()
  
}) // End jQuery Ready

SI.initMap = function () {
  SI.map = new GMaps({
    div: '.map',
    zoom: 13,
    lat: -34.89888467073924,
    lng: -56.16434097290039,
    click: function (event) {
      if (SI.map.markers.length < 1) {
        var lat = event.latLng.lat()
        var lng = event.latLng.lng()
        SI.map.addMarker({
          lat: lat,
          lng: lng
        })
        $('.js-shop-lat').val(lat)
        $('.js-shop-lng').val(lng)
      }
    }
  })

  var currentLat = $('.js-shop-lat').val()
  var currentLng = $('.js-shop-lng').val()

  if (currentLat && currentLng) {
    SI.map.addMarker({
      lat: currentLat,
      lng: currentLng
    })
    SI.map.setCenter(currentLat, currentLng)
  }
}
