/* globals $ */

function makeDroppable (element, callback) {
  var input = document.createElement('input')
  input.setAttribute('type', 'file')
  input.setAttribute('name', 'logo')
  input.classList.add('logo')

  input.addEventListener('change', triggerCallback)

  element.addEventListener('dragover', function (e) {
    e.preventDefault()
    e.stopPropagation()
    element.classList.add('dragover')
  })

  element.addEventListener('dragleave', function (e) {
    e.preventDefault()
    e.stopPropagation()
    element.classList.remove('dragover')
  })

  element.addEventListener('drop', function (e) {
    // e.preventDefault()
    // e.stopPropagation()
    // element.classList.remove('dragover')
    // element.classList.add('dragdone')
    // triggerCallback(e)
  })

  element.addEventListener('click', function () {
    input.value = null
    input.click()
  })

  element.appendChild(input)

  function triggerCallback (e) {
    var files
    if (e.dataTransfer) {
      files = e.dataTransfer.files
    } else if (e.target) {
      files = e.target.files
    }
    callback(files)
  }
}

function callback (files) {
  var file = files[0]
  console.log('files', files)
  console.log('ARCHIVO', $('.logo')[0].files[0])
  var error = false
  if (file.size >= 1000000) {
    error = true
  }

  if (file.type.indexOf('jpeg') === -1 && file.type.indexOf('jpg') === -1 && file.type.indexOf('png') === -1) {
    error = true
  }

  if (error) {
    $('.droppable .hint').text('El archivo debe ser formato PNG o JGP, máximo 1MB')
    $('.droppable .joya').addClass('hidden')
    $('.droppable').removeClass('ok')
    $('.droppable').addClass('error')
  } else {
    $('.droppable .hint').text(file.name)
    $('.droppable .joya').removeClass('hidden')
    $('.droppable').removeClass('error')
    $('.droppable').addClass('ok')
  }
}

$(document).ready(function () {
  $('.js-close-map').on('click', function () {
    $('.js-shop-map-modal').removeClass('active')
  })

  $(document).keyup(function (e) {
    if (e.keyCode === 27) {
      $('.js-shop-map-modal').removeClass('active')
    }
  })

  $('.js-show-map').on('click', function () {
    $('.js-shop-map-modal').addClass('active')
  })

  if ($('.form-item-logo').length > 0) {
    var element = document.querySelector('.droppable')
    makeDroppable(element, callback)
  }
  $('.si-search').select2({
    placeholder: 'Ej. Ploteo, Volante, Serigrafía',
    maximumSelectionLength: 3,
    language: {
      'noResults': function () {
        return 'No hay resultados.'
      },
      'maximumSelected': function () {
        return 'Podés buscar hasta 3 tipos de trabajo.'
      }
    }
  })
  $('.suggest-search').select2({
    language: {
      'noResults': function () {
        return 'No hay resultados.'
      }
    }
  })
}) // End Ready
