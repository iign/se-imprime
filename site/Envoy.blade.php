@servers(['vm' => '-p 2222 vagrant@127.0.0.1', 'server' => 'montag@montag.webfactional.com'])

@task('sync_to_server_beta', ['on' => 'vm'])
    cd Projects/seimprime/site
    gulp
    cd ..
    sh deploy-beta.sh
@endtask

@task('optimize_beta', ['on' => 'server'])
	cd /home/montag/webapps/seimprime_beta
    php55 artisan migrate
    php55 artisan optimize
@endtask

@macro('deploy-beta')
    sync_to_server_beta
    optimize_beta
@endmacro

@task('sync_to_server_production', ['on' => 'vm'])
    cd Projects/seimprime/site
    gulp --production
    cd ..
    sh deploy-prod.sh
@endtask

@task('optimize_prod', ['on' => 'server'])
    cd /home/montag/webapps/seimprime
    php70 artisan optimize
@endtask

@macro('deploy-prod')
    sync_to_server_production
    optimize_prod
@endmacro
