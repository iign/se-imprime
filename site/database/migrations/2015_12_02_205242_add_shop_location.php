<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('shops', function ($table) {
             $table->string('lng', 120)->after('featured')->nullable();
             $table->string('lat', 120)->after('featured')->nullable();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('lat');
         Schema::drop('lng');
     }
}
