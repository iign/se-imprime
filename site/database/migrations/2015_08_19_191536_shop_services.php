<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('service_shop', function (Blueprint $table) {
          $table->integer('shop_id')->unsigned()->index();
          $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');

          $table->integer('service_id')->unsigned()->index();
          $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_shop');
    }
}
