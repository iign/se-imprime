<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;
use App\Shop;

class CreateShopsSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function ($table) {
            $table->string('slug', 120)->after('name');
        });

        $items = Shop::all();
		foreach( $items as $item )
		{
		    $item->slug = Str::limit( Str::slug( $item->name ), 120, '' );
		    $item->save();
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slug');
    }
}
