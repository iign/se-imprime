<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
          'id'   => 1,
          'name' => 'Rubro',
      ]);
      DB::table('categories')->insert([
          'id'   => 2,
          'name' => 'Tarea',
      ]);
      DB::table('categories')->insert([
          'id'   => 3,
          'name' => 'Pieza',
      ]);
      DB::table('categories')->insert([
          'id'   => 4,
          'name' => 'Terminaciones',
      ]);
      DB::table('categories')->insert([
          'id'   => 5,
          'name' => 'Material',
      ]);
    }
}
