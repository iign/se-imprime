<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('services')->insert([
          'name' => 'Imprenta',
          'category_id' => '1',
      ]);
      DB::table('services')->insert([
          'name' => 'Cartelería',
          'category_id' => '1',
      ]);
      DB::table('services')->insert([
          'name' => 'Insumos',
          'category_id' => '1',
      ]);
      DB::table('services')->insert([
          'name' => 'Impresiones',
          'category_id' => '1',
      ]);
    }
}
