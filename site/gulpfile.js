var elixir = require('laravel-elixir')

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
  'jquery': './node_modules/jquery/dist/jquery.min.js',
  'bootstrap': './node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js'
}

elixir(function (mix) {
  // Styles
  mix.copy('node_modules/dropzone/dist/basic.css', 'public/css/dropzone.css')
  .sass('app.scss')
  .sass('admin.scss')

  // Scripts
  // Frontend
  .scripts([paths.jquery, 'resources/assets/js/app.js'], 'public/js/all.js', './')
  // Admin
  .scripts([paths.jquery, paths.bootstrap, 'resources/assets/js/gmaps.js', 'resources/assets/js/admin.js'], 'public/js/admin.js', './') })

//
//
//
// elixir(function(mix) {
//   mix.scripts(["components/jquery/dist/jquery.min.js",
//                "components/mustache/mustache.js",
//                "components/jquery-timeago/jquery.timeago.js",
//                "components/store.js/store.min.js",
//                "js/app.js"],
//                "public/js/all.js",
//                "public")
//      .version(["public/css/main.css", "public/js/all.js"])
//      .copy("public/img", "public/build/img")
// });
//
