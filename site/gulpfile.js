var elixir = require('laravel-elixir')

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
  'jquery': './node_modules/jquery/dist/jquery.min.js',
  'bootstrap': './node_modules/bootstrap-sass',
  'select2': './node_modules/select2/dist/js/select2.min.js'
}

elixir(function (mix) {
  // Styles
  mix.sass(['app.scss'], 'public/css/_app.css')
     .sass(['admin.scss'], 'public/css/_admin.css')
     .styles(['./node_modules/select2/dist/css/select2.min.css', 'public/css/_app.css'], 'public/css/app.css', './')
     .styles(['./node_modules/select2/dist/css/select2.min.css', 'public/css/_admin.css'], 'public/css/admin.css', './')
     .version(['public/css/admin.css', 'public/css/app.css'])
     .copy(paths.bootstrap + '/assets/fonts', 'public/build/fonts')

  // Scripts
  // Frontend
  .scripts([paths.jquery, paths.select2, 'resources/assets/js/app.js'], 'public/js/all.js', './')
  // Admin
  .scripts([paths.jquery, paths.bootstrap + '/assets/javascripts/bootstrap.min.js', paths.select2, 'resources/assets/js/gmaps.js', 'resources/assets/js/admin.js'], 'public/js/admin.js', './') })
