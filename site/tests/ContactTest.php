<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactTest extends TestCase
{
    public function testContact()
    {
        $this->visit('/contacto')
             ->type('Ernesto Prueba', 'contact-name')
             ->type('ernesto@gmail.com', 'contact-email')
             ->type('Hola, este es un mensaje de prueba.', 'contact-message')
             ->press('enviar')
             ->see('Gracias');
    }
}
