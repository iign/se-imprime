<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomeTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testHome()
    {
        $this->visit('/')->see('EMPRESAS RECIENTES');
    }

    public function testListado()
    {
        $this->visit('/')
             ->click('Listado de comercios')
             ->seePageIs('/listado');
    }

    public function testSugeri()
    {
        $this->visit('/')
             ->click('Sugerí')
             ->seePageIs('/sugeri');
    }

    public function testContacto()
    {
        $this->visit('/')
             ->click('Contacto')
             ->seePageIs('/contacto');
    }

    public function testAcerca()
    {
        $this->visit('/')
             ->click('Acerca De')
             ->seePageIs('/acerca');
    }
}
